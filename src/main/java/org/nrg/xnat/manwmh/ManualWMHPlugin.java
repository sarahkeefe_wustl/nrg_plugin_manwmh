package org.nrg.xnat.manwmh;

import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;
import org.nrg.xdat.bean.ManwmhManwmhdataBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@XnatPlugin(value = "nrg_plugin_manwmh", name = "XNAT 1.7 MSIT WMH Thresholding Plugin", description = "This is the XNAT 1.7 MSIT WMH Thresholding Plugin.",
        dataModels = {@XnatDataModel(value = ManwmhManwmhdataBean.SCHEMA_ELEMENT_NAME,
                singular = "MSIT-WMH",
                plural = "MSIT-WMHs")})
@ComponentScan({"org.nrg.xnat.workflow.listeners"})
public class ManualWMHPlugin {
}